import java.util.Scanner;
import java.util.function.Function;

/**
 * A calculator for determining how long
 *
 * @author Daniel Tang
 * @since 6 April 2017
 */
public class TimeToTextMsg {

	@SuppressWarnings("PointlessArithmeticExpression")
	private static final class PhoneNumberTyper {
		/**
		 * Array acting as a map from letter as {@code char} to String of letters typed.
		 * Index is for letter minus 'a'.
		 */
		private static final String[] dataMap;

		static {
			/*
			 * Create and populate an array with how
			 * letters would be typed on an old phone,
			 */
			dataMap = new String['z' - 'a' + 1];

			dataMap['a' - 'a'] = "2";
			dataMap['b' - 'a'] = "22";
			dataMap['c' - 'a'] = "222";
			dataMap['d' - 'a'] = "3";
			dataMap['e' - 'a'] = "33";
			dataMap['f' - 'a'] = "333";
			dataMap['g' - 'a'] = "4";
			dataMap['h' - 'a'] = "44";
			dataMap['i' - 'a'] = "444";
			dataMap['j' - 'a'] = "5";
			dataMap['k' - 'a'] = "5";
			dataMap['l' - 'a'] = "555";
			dataMap['m' - 'a'] = "6";
			dataMap['n' - 'a'] = "66";
			dataMap['o' - 'a'] = "666";
			dataMap['p' - 'a'] = "7";
			dataMap['q' - 'a'] = "77";
			dataMap['r' - 'a'] = "777";
			dataMap['s' - 'a'] = "7777";
			dataMap['t' - 'a'] = "8";
			dataMap['u' - 'a'] = "88";
			dataMap['v' - 'a'] = "888";
			dataMap['w' - 'a'] = "9";
			dataMap['x' - 'a'] = "99";
			dataMap['y' - 'a'] = "999";
			dataMap['z' - 'a'] = "9999";
		}

		/**
		 * Determines the sequence of keys typed for a letter char.
		 *
		 * @param letter The ascii equivalent number of letter.
		 * @return A String representing the keys that need to be pressed.
		 */
		static final String getKeysForLetter(final int letter) {
			if ((letter < 'a') || (letter > 'z')) {
				// Only letters on phone are supported
				throw new IllegalArgumentException("Letter does not appear on phone!");
			}

			/*
			 * Array is offset so that all of the indices are used.
			 * Offset is so that 'a' is at the start of the array.
			 */
			return dataMap[letter - 'a'];
		}

		private PhoneNumberTyper() { throw new UnsupportedOperationException(); }
	}

	public static void main(final String[] ignore) {
		@SuppressWarnings("resource")
		final Scanner scanner = new Scanner(System.in);

		/*
		 * Accept typed phrases until we reach "halt",
		 * when each phrase is received,
		 * process it by calculating and outputing
		 * the time to type.
		 */
		while (true) {
			final String input = scanner.nextLine();

			if (input.equals("halt")) {
				return;
			}

			System.out.println(calculateTime(input));
		}
	}

	/**
	 * Function class to apply to String stream, to insert required pauses.
	 */
	private static final class Mapper implements Function<String, String> {
		/**
		 * Stores last digit, to determine if it is the same as the next.
		 */
		char lastKey = '\uFFFF'; // Init to impossible character

		/**
		 * Actual logic for inserting pauses.
		 * Runs through streams and intercept letters using the same key side to side, and inserts pauses.
		 *
		 * @param str A String of keys that need to be pressed.
		 * @return A String of keys that need to be pressed, with ".." inserted if a 2 second wait is needed.
		 */
		@Override
		public final String apply(String str) {
			/*
			 * 1 digit = 1 second
			 * We need to wait 2 seconds to pause for
			 * consecutive letters using the same key.
			 * So insert 2 nonsense digits, to increase the time.
			 */
			if (lastKey == str.charAt(0)) {
				str = ".." + str;
			}

			//Store the last character, for next check.
			lastKey = str.charAt(str.length() - 1);

			return str;
		}
	}

	/**
	 * Calculates the time it takes to text
	 *
	 * @param msg The message to be typed
	 * @return The time to type the message, each letter takes 1 second, consecutive letters need a 2 second.
	 */
	private static int calculateTime(final String msg) {
		return msg.toLowerCase() // Case doesn't matter
				.replaceAll("[^a-z]", "") // Remove all punctuation and other non-letters
				.chars() // Stream
				.mapToObj(PhoneNumberTyper::getKeysForLetter) // Convert to keys typed
				.map(new Mapper()) // Insert pauses
				.mapToInt(String::length) // Calculate lengths
				.sum(); // Calculate total length
	}
}
